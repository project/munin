<?php
/**
 * @file
 *  Munin API Admin forms.
 */


/**
 * Settings form.
 *
 * @return array
 *  System settings form array.
 */
function munin_api_settings_form() {
  $form = array();

  // Helper.
  $form['help'] = array(
    '#value' => '<p>' . t('Save this form, then go to the !plugins tab to select and configure your plugins.',
        array('!plugins' => l(t('Plugins'), 'admin/settings/munin/plugins'))) . '</p>',
  );

  // General settings.
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
  );
  $form['general'][MUNIN_API_VAR_CATEGORY] = array(
    '#type' => 'textfield',
    '#title' => t('The category name for the site'),
    '#default_value' => variable_get(MUNIN_API_VAR_CATEGORY, ''),
    '#required' => FALSE,
    '#description' => t('Leave this blank to use the absolute URL to the root of the site. Recommended.'),
  );
  $form['general'][MUNIN_API_VAR_TIMEFRAME] = array(
    '#type' => 'textfield',
    '#title' => t('The timeframe used for delta graphs measuring change over time.'),
    '#default_value' => variable_get(MUNIN_API_VAR_TIMEFRAME, ''),
    '#required' => FALSE,
    '#description' => t('Set the timeframe in seconds for sampling.'),
  );

  // Drush settings.
  $form['drush'] = array(
    '#type' => 'fieldset',
    '#title' => t('Drush'),
  );
  $form['drush'][MUNIN_API_VAR_DRUSH_PATH] = array(
    '#type' => 'textfield',
    '#title' => t('Drush executable'),
    '#default_value' => variable_get(MUNIN_API_VAR_DRUSH_PATH, 'drush'),
    '#required' => TRUE,
    '#description' => t('Path to Drush executable'),
  );
  $drush_users = array('www-data' => 'www-data', 'apache' => 'apache', 'aegir' => 'aegir', 'custom' => 'custom');
  $form['drush'][MUNIN_API_VAR_DRUSH_USER] = array(
    '#type' => 'select',
    '#title' => t('Drush user'),
    '#options' => $drush_users,
    '#default_value' => variable_get(MUNIN_API_VAR_DRUSH_USER, 'www-data'),
    '#required' => TRUE,
    '#description' => t('The user drush should run as.'),
  );
  $form['drush'][MUNIN_API_VAR_DRUSH_USER_CUSTOM] = array(
    '#type' => 'textfield',
    '#title' => t('Custom drush user'),
    '#default_value' => variable_get(MUNIN_API_VAR_DRUSH_USER_CUSTOM, ''),
    '#required' => FALSE,
    '#description' => t('A custom drush user.'),
    '#states' => array(
      'visible' => array(
       ':input[name="' . MUNIN_API_VAR_DRUSH_USER . '"]' => array('value' => 'custom'),
      ),
    )
  );
  $form['drush'][MUNIN_API_VAR_DRUSH_ALIAS] = array(
    '#type' => 'textfield',
    '#title' => t('Drush alias'),
    '#default_value' => variable_get(MUNIN_API_VAR_DRUSH_ALIAS, ''),
    '#description' => t('Will override the values set for root and uri. Helpful when the root changes, such as on Aegir systems. Omit the @. Make sure the alias is available to the user running the script.'),
  );
  $form['drush'][MUNIN_API_VAR_DRUSH_ROOT] = array(
    '#type' => 'textfield',
    '#title' => t('Drush root'),
    '#default_value' => variable_get(MUNIN_API_VAR_DRUSH_ROOT,
        (isset($_SERVER['DOCUMENT_ROOT']) ? $_SERVER['DOCUMENT_ROOT'] : '')),
    '#description' => t('As passed to <strong>--root<strong> parameter'),
    '#states' => array(
      'invisible' => array(
       ':input[name="' . MUNIN_API_VAR_DRUSH_ALIAS . '"]' => array('filled' => TRUE),
      ),
    )
  );
  $form['drush'][MUNIN_API_VAR_DRUSH_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Drush URI'),
    '#default_value' => variable_get(MUNIN_API_VAR_DRUSH_URI, 'default'),
    '#description' => t('As passed to <strong>--uri<strong> parameter'),
    '#states' => array(
      'invisible' => array(
       ':input[name="' . MUNIN_API_VAR_DRUSH_ALIAS . '"]' => array('filled' => TRUE),
      ),
    )
  );

  // Go to the scripts after saving.
  $form['#redirect'] = 'admin/settings/munin/plugins';

  // Save settings automatically.
  return system_settings_form($form);
}

/**
 * Validate the munin_settings_form.
 */
function munin_api_settings_form_validate($form, $form_state) {
  $values = $form_state['values'];
  // Check that a custom value has been selected if custom user.
  if ($values[MUNIN_API_VAR_DRUSH_USER] == 'custom' && empty($values[MUNIN_API_VAR_DRUSH_USER_CUSTOM])) {
    form_set_error(MUNIN_API_VAR_DRUSH_USER_CUSTOM, t('Custom user field is missing.'));
  }
  // Check that uri and root have been selected if there is no alias.
  if (empty($values[MUNIN_API_VAR_DRUSH_ALIAS])) {
    if (empty($values[MUNIN_API_VAR_DRUSH_ROOT])) {
      form_set_error(MUNIN_API_VAR_DRUSH_ROOT, t('Complete the Drush Root field if not using an alias.'));
    }
    if (empty($values[MUNIN_API_VAR_DRUSH_URI])) {
      form_set_error(MUNIN_API_VAR_DRUSH_URI, t('Complete the Drush URI field if not using an alias.'));
    }
  }
}

/**
 * Settings form.
 *
 * @return array
 *  System settings form array.
 */
function munin_api_plugins_form() {
  $form = array();

  // Helper.
  $form['help'] = array(
    '#value' => '<p>' . t('Save this form, then go to the !scripts tab to copy the shell script code.',
        array('!scripts' => l(t('Scripts'), 'admin/settings/munin/scripts'))) . '</p>',
  );

  // Saved plugin configs [on/off].
  $plugin_defaults = variable_get(MUNIN_API_VAR_PLUGINS, array());
  // Full plugin definitions.
  $plugins = munin_api_plugin_definitions();

  if (count($plugins) == 0) {
    $form['message'] = array(
      '#type' => 'markup',
      '#value' => t('There are currently no munin plugin definitions available. Please enable a module that defines some munin plugin configurations.'),
    );
  }
  else {
    $form['munin_plugins'] = array(
      '#tree' => TRUE,
      '#type' => 'vertical_tabs'
    );
    foreach ($plugins as $category => $definition) {
      // There are some meta elements describing the category All remaining
      // elements are definitions.
      $form['munin_plugins'][$category] = array(
        '#type' => 'fieldset',
        '#title' => check_plain($definition['#title']),
        '#description' => check_plain($definition['#description']),
        '#collabsible' => TRUE,
        '#collapsed' => TRUE,
      );
      foreach ($definition as $key => $value) {
        // Each plugin may define multiple stats in their category. Each of
        // them represents an item in the munin graph element. Let the user
        // choose.
        if (drupal_substr($key, 0, 1) != '#') {
          $form['munin_plugins'][$category][$key] = array(
            '#type' => 'fieldset',
            '#title' => check_plain($value['#label']),
            '#description' => empty($value['#description']) ? '': check_plain($value['#description']),
            '#collabsible' => TRUE,
            '#collapsed' => FALSE,
            '#tree' => TRUE,
          );
          $form['munin_plugins'][$category][$key]['enable'] = array(
            '#type' => 'checkbox',
            '#title' => t('Enable'),
            '#default_value' => isset($plugin_defaults[$category][$key]['enable'])?
              $plugin_defaults[$category][$key]['enable']:'',
          );
          $form['munin_plugins'][$category][$key]['warning'] = array(
            '#type' => 'textfield',
            '#title' => t('Warning level'),
            '#description' => t('Warning threshold level in Munin reports.'),
            '#size' => 5,
            '#default_value' => _munin_plugin_default($category, $key, 'warning', $plugin_defaults, $plugins),
          );
          $form['munin_plugins'][$category][$key]['critical'] = array(
            '#type' => 'textfield',
            '#title' => t('Critical level'),
            '#description' => t('Critical threshold level in Munin reports.'),
            '#size' => 5,
            '#default_value' => _munin_plugin_default($category, $key, 'critical', $plugin_defaults, $plugins),
          );
        }
      }
    }
  }

  // Go to the scripts after saving.
  $form['#redirect'] = 'admin/settings/munin/scripts';

  // Save settings automatically.
  return system_settings_form($form);
}

/**
 * Page callback of results.
 */
function munin_api_results_page() {
  drupal_add_library('system', 'drupal.collapse');
  // Loop through the plugins
  $s = array();
  foreach (munin_api_plugin_definitions(TRUE) as $plugin_name => $plugin) {
    $s[] = '<fieldset class="collapsible collapsed">' .
      '<legend><span class="fieldset-legend"><a href="#">' . check_plain($plugin['#title']) . '</a></span></legend>' .
        '<div class="fieldset-wrapper">' .
        '<h3>' . t('Configuration') . '</h3>' .
        '<pre>$ drush munin-run ' . check_plain($plugin_name) . ' --get-config</pre>' .
        '<pre>' . munin_api_get_config($plugin, $plugin_name) . '</pre>' .
        '<h3>' . t('Values') . '</h3>' .
        '<pre>$ drush munin-run ' . check_plain($plugin_name) . '</pre>' .
        '<pre>' . munin_api_get_values($plugin, $plugin_name) . '</pre>' .
      '</div>' .
    '</fieldset>';
  }
  return implode($s);
}

/**
 * Page callback of rendered script.
 */
function munin_api_script_page() {
  $output = '<h2>' . t('1. Manual installation') . '</h2>' .
    '<p>' . t('Follow the step by step instructions from the README and install this file.') . '</p>' .
    '<pre>' . munin_api_script() . '</pre>';
  $output .= '<h2>' . t('2. Install with help from drush (recommended)') . '</h2>' .
    '<p>' . t('Pipe a script generated by Drush into the shell. This allows to to run the script as root without involving Drush. You should review the contents of the script before running it.') . '</p>' .
    '<pre>' .
    "$ drush munin-setup | sudo sh\n" .
    '</pre>';
  return $output;
}

/**
 * Helper which looks for a default value for vars in the form. It will first
 * try the munin_plugins variable. If that fails, it will try the actual plugin
 * definition. This ensures that defaults for "critical" and "warning" which are
 * set in the plugin are availabe to the user for editing on the first time they
 * come to the form. If the user wants to blank it out, they can.
 *
 */
function _munin_plugin_default($category, $key, $var, $plugin_defaults, $plugins) {
  // If there is a saved value (non blank) then that is the defaut
  if (!empty($plugin_defaults[$category][$key][$var])) {
    return $plugin_defaults[$category][$key][$var];
  }
  // If the form has been saved before (blank value) and plugin has a value
  if (!isset($plugin_defaults[$category][$key][$var]) && !empty($plugins[$category][$key]['#config'][$var])) {
    return $plugins[$category][$key]['#config'][$var];
  }
  // A blank value has been saved before.
  return '';
}
